import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Button } from "../components/Button";
import { Container } from "../components/Container";
import { Dropdown } from "../components/Dropdown";
import { Heading3 } from "../components/Heading";
import { Table, Th, Thead, Tr } from "../components/Table";
import { TextField } from "../components/TextField";
import { addTaskAction, updateTask } from "../redux/action/ToDoListAction";
import { arrTheme } from "../themes/ThemeManager";
import { changeThemeAction } from "../redux/action/ToDoListAction";
import { doneTaskAction } from "../redux/action/ToDoListAction";
import { deleteTaskAction } from "../redux/action/ToDoListAction";
import { refresh } from "../redux/action/ToDoListAction";
import { editTaskAction } from "../redux/action/ToDoListAction";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  RenderTaskToDoList = () => {
    return this.props.TaskList.filter((task) => !task.done).map(
      (task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                  this.props.dispatch(editTaskAction(task));
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      }
    );
  };
  renderComPleted = () => {
    return this.props.TaskList.filter((task) => task.done).map(
      (task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(editTaskAction(task));
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(refresh(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-refresh"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      }
    );
  };
  renderTheme = () => {
    return arrTheme.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };
  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(changeThemeAction(value));
              this.props.dispatch({
                type: "change_theme",
                themeId: value,
              });
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To Do List</Heading3>
          <TextField
            value={this.state.taskName}
            onChange={(e) => {
              this.setState({ taskName: e.target.value }, () => {
                console.log(this.state);
              });
            }}
            label="Task name"
            className="w-50"
          />
          <Button
            onClick={() => {
              let { taskName } = this.state;
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log(newTask);
              this.props.dispatch(addTaskAction(newTask));
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i> Add task
          </Button>
          {this.state.disabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.dispatch(updateTask(this.state.taskName));
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update Task
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    disabled: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTask(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update Task
            </Button>
          )}
          <Heading3>Task To List</Heading3>
          <Table>
            <Thead>{this.RenderTaskToDoList()}</Thead>
          </Table>
          <Heading3>Task</Heading3>
          <Table>
            <Thead>{this.renderComPleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}
let mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    TaskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};
export default connect(mapStateToProps)(ToDoList);
